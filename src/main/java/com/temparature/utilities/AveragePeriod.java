package com.temparature.utilities;

public enum AveragePeriod  {

	monthly("monthly"), weekly("weekly"), daily("daily");
	
	private String val;
	
	private AveragePeriod(String value) {
		this.val = value;
	}

	public String value() {
		return this.val;
	}
	
	public String validValues() {
		return "['monthly', 'weekly', 'daily']";
	}
	
}
