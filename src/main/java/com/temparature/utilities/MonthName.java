/**
 * 
 */
package com.temparature.utilities;

/**
 * this enum is to find the naem of the month from the date stored
 * @author shtiwari
 *
 */
public enum MonthName {
	
	month_1("January", 0), 
	month_2("February", 1), 
	month_3("March", 2), 
	month_4("April", 3), 
	month_5("May", 4), 
	month_6("June", 5), 
	month_7("July", 6), 
	month_8("Auguest", 7), 
	month_9("September", 8), 
	month_10("October", 9), 
	month_11("November", 10), 
	month_12("December", 11);

	private String val;
	private int monthNum;
	
	MonthName( String val, int monthNum ) {
		this.val = val;
		this.monthNum = monthNum;
	}
	
	public String value() {
		return this.val;
	}
	
	public int getMonthNumber() {
		return this.monthNum;
	}
}