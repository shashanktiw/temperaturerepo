package com.temparature.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class LocationValidator {
	
	private static List<String> location_list ;
	
	public LocationValidator() {
		location_list = fetchAllLocations();
	}
	
	private List<String> fetchAllLocations() {
		List<String> allLocations = new ArrayList<>(Arrays.asList("delhi", "gurgaon", "noida"));
		return allLocations;
	}
	
	public boolean isLocationValid(String loc) {
		return location_list.contains(loc);
	}
}
