package com.temparature.utilities;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * this class provides the utility methods to generate the JSON responses.
 * 
 * @author shtiwari
 *
 */
public class JsonMessageGenerator {

	private static final int INVALID_DATA_ERROR_CODE = 422;
	private static final int NO_DATA_ERROR_CODE = 204;
	private static final int UNKNOWN_DATA_ERROR_CODE = 503;

	/**
	 * validate the location from the existing list 
	 * if invalid returns the error message
	 *  
	 * @param loc
	 * @return
	 * @throws JSONException
	 */
	public static String inputLocationNotValid( String loc ) throws JSONException {
		String errorMessage = "Provided location " + loc + " is invalid.";
		return genericErrorMessage(INVALID_DATA_ERROR_CODE, errorMessage);
	}
	
	/**
	 * this will be used to notify user that the data for location provided is not present in DB 
	 * 
	 * @param loc
	 * @return
	 * @throws JSONException
	 */
	public static String dataNotPresent( String loc ) throws JSONException {
		String errorMessage = "No data present for location " + loc ;
		return genericErrorMessage(NO_DATA_ERROR_CODE, errorMessage);
	}
	
	/**
	 * creates a json obj with error code and message provided
	 * 
	 * @param errorCode
	 * @param errorMessage
	 * @return
	 * @throws JSONException
	 */
	public static String genericErrorMessage( int errorCode, String errorMessage ) throws JSONException {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("errorCode", errorCode);
		jsonObj.put("message", errorMessage );
		
		return jsonObj.toString();
	}

	/**
	 * generic error message creator
	 * 
	 * @return
	 * @throws JSONException
	 */
	public static String genericErrorMessage() throws JSONException {
		return genericErrorMessage(UNKNOWN_DATA_ERROR_CODE, "Try Again");
	}

	/**
	 * creates json message for invalid date
	 * @return
	 * @throws JSONException
	 */
	public static String invalidDateException() throws JSONException {
		return genericErrorMessage(INVALID_DATA_ERROR_CODE, "Invalid date entered. Validate the format as " + Util.DATE_FORMAT_STRING );
	}

	/**
	 * creates json message for invalid period
	 * @return
	 * @throws JSONException
	 */
	public static String invalidPeriodException() throws JSONException {
		return genericErrorMessage(INVALID_DATA_ERROR_CODE, "Invalid period entered. Valid values for period are " + AveragePeriod.daily.validValues() );
	}
	
}
