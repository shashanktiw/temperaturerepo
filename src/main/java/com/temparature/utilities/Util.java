package com.temparature.utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;

import com.temparature.beans.TempDataFromSensor;

public class Util {

	public static final String MONTH_PREFIX = "month_";
	public static final String HOUR_PREFIX = "hour_";
	public static final String MINUTE_PREFIX = "minute_";
	public static final String SECOND_PREFIX = "second_";
	public static final String DAY_PREFIX = "day_";
	
	public final static String DATE_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss";
	public final static DateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_STRING, Locale.ENGLISH);
	
	public static Calendar parseDateString(String dateStr) {
		DATE_FORMAT.setLenient(false);
		Date date = null;
		try {
			date = DATE_FORMAT.parse(dateStr);
		} catch (ParseException e) {
			//exception while parsing date
			e.printStackTrace();
		}

		if (date != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			return cal;
		} else {
			return null;
		}
	}
	
	/**
	 * this method is to validate the input from the sensor
	 * @param tds
	 * @return
	 */
	public static String findInValidValueInSensorData( TempDataFromSensor tds ) {
		
		String invalidData = null;
		
		// validate date or timestamp is in correct format 
		String timeStamp = tds.getTimeStamp();
		Calendar cal = parseDateString(timeStamp);
		if (cal == null) {
			// date not in correct format.
			try {
				return JsonMessageGenerator.invalidDateException();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
		
		return invalidData;
	}
	
}
