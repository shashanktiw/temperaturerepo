package com.temparature.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.temparature.beans.TempDataFromSensor;
import com.temparature.morpher.TemperatureDM;

/**
 * This is the main entry point.
 * this has all the rest API that are available to interact with the system
 * 
 * @author shtiwari
 *
 */

@RestController
public class TemperatureController {
	
	@Autowired
	TemperatureDM tDm;

	/**
	 * this API fetches all the temp data for all the locations
	 * @return
	 */
	@GetMapping(value="/temp", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getAllTemp() {
		return tDm.getAllLocationData();
	}
	
	/**
	 * this API fetches all the temp data for a particular location given by the user
	 * 
	 * @param locationId
	 * @return
	 */
	@GetMapping(value="/temp/{locationId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getTemp(@PathVariable(value = "locationId") String locationId) {
		return tDm.getTemperatureByLocationId(locationId);
	}
	
	/**
	 * this API takes the data and put in the mongo db after verification 
	 * @param inputData
	 * @return
	 */
	@PostMapping(value="/temp", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object addTemp( @RequestBody TempDataFromSensor inputData ) {
		return tDm.addNewTemperatureDataPoint(inputData);
	}
	
	
	/**
	 * this API fetches all the temp data for a particular location given by the user
	 * 
	 * @param locationId
	 * @return
	 */
	@GetMapping(value="/temp/{locationId}/average/{period}", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getTemp(@PathVariable(value = "locationId") String locationId, @PathVariable(value = "period") String period) {
		return tDm.getAverageData(locationId, period);
	}
	
}