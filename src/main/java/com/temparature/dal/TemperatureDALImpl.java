package com.temparature.dal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.temparature.beans.TemperatureDataPoint;

/**
 * this class is the implementation for the data access layer
 * @author shtiwari
 *
 */
@Repository
public class TemperatureDALImpl implements TemperatureDAL {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<TemperatureDataPoint> getAllLocations() {
		return mongoTemplate.findAll(TemperatureDataPoint.class);
	}

	@Override
	public TemperatureDataPoint getTemperatureByLocationId(String locationId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("locationId").is(locationId));
		return mongoTemplate.findOne(query, TemperatureDataPoint.class);
	}

	@Override
	public TemperatureDataPoint addNewTemperatureDataPoint(TemperatureDataPoint tdpFromDb) {
		mongoTemplate.save(tdpFromDb);
		return tdpFromDb;

	}

}
											