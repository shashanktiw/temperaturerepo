package com.temparature.dal;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.temparature.beans.TemperatureDataPoint;

/**
 * this class is to access the data base
 * 
 * @author shtiwari
 *
 */
@Repository
public interface TemperatureDAL {
	
	/**
	 * this method returns all data for all locations 
	 * @return
	 */
	List<TemperatureDataPoint> getAllLocations();

	/**
	 * this returns the all temp data for 1 particular location 
	 * @param locationId
	 * @return
	 */
	TemperatureDataPoint getTemperatureByLocationId(String locationId);

	/**
	 * this method add new data point or temp information
	 * 
	 * @param tdp
	 * @return
	 */
	TemperatureDataPoint addNewTemperatureDataPoint(TemperatureDataPoint tdp);

}
