package com.temparature.beans;

public class TempDataFromSensor {
	
	private String locationId;
	private String timeStamp;
	private float temp;
	
	/**
	 * default constructor
	 */
	public TempDataFromSensor() {}
	
	/**
	 * parameterised constructor
	 * @param locationId
	 * @param timeStamp
	 * @param temp
	 */
	public TempDataFromSensor(String locationId, String timeStamp, float temp) {
		super();
		this.locationId = locationId;
		this.timeStamp = timeStamp;
		this.temp = temp;
	}

	@Override
	public String toString() {
		String tmp = "Location :: " + this.locationId + " polled at " + this.timeStamp + " reported temp :: " + this.temp;
		return tmp;
	}

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param lastPolledTimeStamp the lastPolledTimeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the temp
	 */
	public float getTemp() {
		return temp;
	}

	/**
	 * @param temp the temp to set
	 */
	public void setTemp(float temp) {
		this.temp = temp;
	}
	
}