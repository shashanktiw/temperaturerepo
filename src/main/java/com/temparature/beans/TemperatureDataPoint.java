package com.temparature.beans;

import java.util.HashMap;
import java.util.Map;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class TemperatureDataPoint {
	
	@Id
	private String locationId;
	private String lastPolledTimeStamp;
	
	//private TemperatureData tempData;
	
	private Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> tempData;

	public TemperatureDataPoint() {
		tempData = new HashMap<>();
	}

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the lastPolledTimeStamp
	 */
	public String getLastPolledTimeStamp() {
		return lastPolledTimeStamp;
	}

	/**
	 * @param lastPolledTimeStamp the lastPolledTimeStamp to set
	 */
	public void setLastPolledTimeStamp(String lastPolledTimeStamp) {
		this.lastPolledTimeStamp = lastPolledTimeStamp;
	}

	/**
	 * @return the tempData
	 */
	public Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> getTempData() {
		return tempData;
	}

	/**
	 * @param tempData the tempData to set
	 */
	public void setTempData(Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> tempData) {
		this.tempData = tempData;
	}

}