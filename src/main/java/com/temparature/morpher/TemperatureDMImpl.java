package com.temparature.morpher;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.temparature.beans.TempDataFromSensor;
import com.temparature.beans.TemperatureDataPoint;
import com.temparature.dal.TemperatureDAL;
import com.temparature.utilities.AveragePeriod;
import com.temparature.utilities.JsonMessageGenerator;
import com.temparature.utilities.LocationValidator;
import com.temparature.utilities.MonthName;
import com.temparature.utilities.Util;

@Repository
public class TemperatureDMImpl implements TemperatureDM {

	@Autowired
	TemperatureDAL tDal;

	@Autowired
	LocationValidator lvd;

	private String retString = "Something went wrong. Please try Again";

	@Override
	public Object getAllLocationData() {
		List<TemperatureDataPoint> allData = tDal.getAllLocations();

		// validate data is found
		//if not then return no data message
		if( (allData == null) || (allData.size()==0) ) {
			// return no data error
			try {
				retString = JsonMessageGenerator.dataNotPresent("");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return retString;
		} else {
			// return the data found
			return allData;
		}
	}

	@Override
	public Object getTemperatureByLocationId(String locationId) {

		// fetch the data
		TemperatureDataPoint tdp = tDal.getTemperatureByLocationId(locationId);

		// validate null, if empty compile notification indicating non existing of data
		if (tdp == null) {
			// no data found, send error message or notification for the same.
			try {
				retString = JsonMessageGenerator.dataNotPresent(locationId);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return retString;
		} else {
			// all good lets change to json string
			return tdp;
		}
	}

	@Override
	public Object addNewTemperatureDataPoint(TempDataFromSensor inData) {

		String invalidData = Util.findInValidValueInSensorData(inData);
		if (invalidData != null) {
			return invalidData;
		}

		// add data in DB
		TemperatureDataPoint tdpFromDb = tDal.getTemperatureByLocationId(inData.getLocationId());

		Calendar timeStamp = Util.parseDateString(inData.getTimeStamp());

		String monthFromReportedData = Util.MONTH_PREFIX + (timeStamp.get(Calendar.MONTH) + 1);
		String dateFromReportedData = Util.DAY_PREFIX + timeStamp.get(Calendar.DATE);
		String hourFromReportedData = Util.HOUR_PREFIX + timeStamp.get(Calendar.HOUR_OF_DAY);
		String minuteFromReportedData = Util.MINUTE_PREFIX + timeStamp.get(Calendar.MINUTE);
		String secondFromReportedData = Util.SECOND_PREFIX + timeStamp.get(Calendar.SECOND);

		if (tdpFromDb == null) {
			// not present in DB
			// create new object to save
			tdpFromDb = new TemperatureDataPoint();
			tdpFromDb.setLocationId(inData.getLocationId());
		}
		// update the polled time stamp
		tdpFromDb.setLastPolledTimeStamp(inData.getTimeStamp());

		// temp":{"month-10":{"date-13":{"hour-2":{"min-5":{"sec-2":10.33,"sec-1":13.33}}}}
		Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> allTempDataArray = tdpFromDb
				.getTempData();

		Map<String, Map<String, Map<String, Map<String, Float>>>> monthData = allTempDataArray
				.get(monthFromReportedData);

		// month data not present, lets create
		if (monthData == null) {
			monthData = new HashMap<>();
			allTempDataArray.put(monthFromReportedData, monthData);
		}

		// date data not present, lets create
		Map<String, Map<String, Map<String, Float>>> dateData = monthData.get(dateFromReportedData);
		if (dateData == null) {
			dateData = new HashMap<>();
			monthData.put(dateFromReportedData, dateData);
		}
		
		// hour data not present, lets create
		Map<String, Map<String, Float>> hourData = dateData.get(hourFromReportedData);
		if (hourData == null) {
			hourData = new HashMap<>();
			dateData.put(hourFromReportedData, hourData);
		}
		
		// min data not present, lets create
		Map<String, Float> minDataMap = hourData.get(minuteFromReportedData);
		if (minDataMap == null) {
			minDataMap = new HashMap<>();
			hourData.put(minuteFromReportedData, minDataMap);
		}
		
		// add the data received
		minDataMap.put(secondFromReportedData, inData.getTemp());

		//save the data now 
		tDal.addNewTemperatureDataPoint(tdpFromDb);
		
		//return the object
		return tdpFromDb;
	}

	@Override
	public Object getAverageData(String locationId, String period) {

		try {
			AveragePeriod.valueOf(period);
		} catch ( IllegalArgumentException ex ) {
			//invalid value for period
			try {
				retString = JsonMessageGenerator.invalidPeriodException();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return retString;
		}
		
		// fetch the data from DB for the location
		TemperatureDataPoint tdpFromDb = tDal.getTemperatureByLocationId(locationId);

		// data not found so return the message
		if ( tdpFromDb == null ) {
			try {
				retString = JsonMessageGenerator.dataNotPresent(locationId);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			return retString;
		}
		
		// map to return the average temperatures
		Map<String, Float> avgTempMap = new HashMap<>();

		// switch according to the period
		switch (period) {
		case "monthly":
			avgTempMap = calculateMonthlyAverage( tdpFromDb.getTempData() );
			break;
		case "weekly":
			avgTempMap = calculateWeeklyAverage( tdpFromDb.getTempData() );
			break;
		case "daily":
			avgTempMap = calculateDailyAverage( tdpFromDb.getTempData() );
			break;
		}

		// return the average temperature
		return avgTempMap;
	}

	/**
	 * this method is to calculate the daily average temp return map key =
	 * <month_name>_<day>, value = average temp
	 * 
	 * @param tempData
	 * @return
	 */
	private Map<String, Float> calculateDailyAverage(
			Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> tempData) {
		// map to return the data
		Map<String, Float> dailyAverageData = new HashMap<>();

		// start reading the data
		// loop 1 == month data
		for (Entry<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> mMap : tempData.entrySet()) {

			String monthName = mMap.getKey();

			// loop 2 == date data
			Map<String, Map<String, Map<String, Map<String, Float>>>> monthlyData = mMap.getValue();
			for (Entry<String, Map<String, Map<String, Map<String, Float>>>> dData : monthlyData.entrySet()) {

				String dateFromData = dData.getKey();
				int dateFromDataInt = Integer.parseInt(dateFromData.substring(Util.DAY_PREFIX.length()));

				// init the counters
				int totalDataPoints = 0;
				float tempTotal = 0;
				float avgTemp = 0;

				// loop 3 == hourly data
				Map<String, Map<String, Map<String, Float>>> dDataVal = dData.getValue();
				for (Entry<String, Map<String, Map<String, Float>>> hData : dDataVal.entrySet()) {
					// loop 4 == minute data
					Map<String, Map<String, Float>> hDataVal = hData.getValue();
					for (Entry<String, Map<String, Float>> mData : hDataVal.entrySet()) {
						// loop 5 == seconds data
						Map<String, Float> mDataVal = mData.getValue();
						for (Entry<String, Float> sData : mDataVal.entrySet()) {
							// Temperature
							tempTotal += sData.getValue();
							totalDataPoints++;
						}
					}
				}
				// calculate the average
				avgTemp = tempTotal / totalDataPoints;

				// e.g. January_1
				String key = MonthName.valueOf(monthName).value() + "_" + dateFromDataInt;
				// put in the map
				dailyAverageData.put(key, avgTemp);
			}
		}

		// return the final data
		return dailyAverageData;
	}

	/**
	 * this method is used to average the Monthly data and report back in the form
	 * of Map <key, val> where key = month name , value = average for that month
	 * 
	 * @param tempData
	 * @return
	 */
	private Map<String, Float> calculateMonthlyAverage(
			Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> tempData) {

		// map to return the data
		Map<String, Float> monthlyAverageData = new HashMap<>();

		// start reading the data
		// loop 1 == month data
		for (Entry<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> mMap : tempData.entrySet()) {
			int totalDataPoints = 0;
			float tempTotal = 0;
			float avgTemp = 0;

			String monthName = mMap.getKey();

			// loop 2 == date data
			Map<String, Map<String, Map<String, Map<String, Float>>>> monthlyData = mMap.getValue();
			for (Entry<String, Map<String, Map<String, Map<String, Float>>>> dData : monthlyData.entrySet()) {
				// loop 3 == hourly data
				Map<String, Map<String, Map<String, Float>>> dDataVal = dData.getValue();
				for (Entry<String, Map<String, Map<String, Float>>> hData : dDataVal.entrySet()) {
					// loop 4 == minute data
					Map<String, Map<String, Float>> hDataVal = hData.getValue();
					for (Entry<String, Map<String, Float>> mData : hDataVal.entrySet()) {
						// loop 5 == seconds data
						Map<String, Float> mDataVal = mData.getValue();
						for (Entry<String, Float> sData : mDataVal.entrySet()) {
							// Temperature
							tempTotal += sData.getValue();
							totalDataPoints++;
						}
					}
				}
			}

			// calculate the average
			avgTemp = tempTotal / totalDataPoints;

			// put in the map
			monthlyAverageData.put(MonthName.valueOf(monthName).value(), avgTemp);
		}

		// return the final data
		return monthlyAverageData;
	}

	/**
	 * this method is used to average the Monthly data and report back in the form
	 * of Map <key, val> where key = week_<Start_date>_<end_date>_month_name, value
	 * = average for that month
	 * 
	 * @param tempData
	 * @return
	 */
	private Map<String, Float> calculateWeeklyAverage(
			Map<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> tempData) {

		// map to return the data
		Map<String, Float> weeklyAverageTempMap = new HashMap<>();
		Map<String, Integer> weeklyDataPoints = new HashMap<>();

		// calendar object to find the day of the week
		Calendar cal = Calendar.getInstance();
		int totalDataPoints = 0;
		float tempTotal = 0;

		// start reading the data
		// loop 1 == month data
		for (Entry<String, Map<String, Map<String, Map<String, Map<String, Float>>>>> mMap : tempData.entrySet()) {
			String monthName = mMap.getKey();
			// update the month from data
			cal.set(Calendar.MONTH, MonthName.valueOf(monthName).getMonthNumber());

			// loop 2 == date data
			Map<String, Map<String, Map<String, Map<String, Float>>>> monthlyData = mMap.getValue();
			for (Entry<String, Map<String, Map<String, Map<String, Float>>>> dData : monthlyData.entrySet()) {
				String dateFromData = dData.getKey();
				int dateFromDataInt = Integer.parseInt(dateFromData.substring(Util.DAY_PREFIX.length()));
				// update the date from data
				cal.set(Calendar.DATE, dateFromDataInt);

				// store the average temp of last week
				int tempDays = cal.get(Calendar.DAY_OF_WEEK);
				int addDays = 7 - tempDays;
				Calendar endCal = Calendar.getInstance();
				endCal.setTimeInMillis(cal.getTimeInMillis());
				endCal.add(Calendar.DATE, addDays);
				int endDateOfWeek = endCal.get(Calendar.DATE);

				Calendar startCal = Calendar.getInstance();
				startCal.setTimeInMillis(cal.getTimeInMillis());
				startCal.add(Calendar.DATE, -tempDays + 1);
				int startDateOfWeek = startCal.get(Calendar.DATE);

				// key = week_<Start_date>_<end_date>_month_name
				String weeklyMapKey = "week_" + startDateOfWeek + "_" + endDateOfWeek + "_"
						+ MonthName.valueOf(monthName).value();

				// loop 3 == hourly data
				Map<String, Map<String, Map<String, Float>>> dDataVal = dData.getValue();
				for (Entry<String, Map<String, Map<String, Float>>> hData : dDataVal.entrySet()) {
					// loop 4 == minute data
					Map<String, Map<String, Float>> hDataVal = hData.getValue();
					for (Entry<String, Map<String, Float>> mData : hDataVal.entrySet()) {
						// loop 5 == seconds data
						Map<String, Float> mDataVal = mData.getValue();
						for (Entry<String, Float> sData : mDataVal.entrySet()) {
							// fetch the data, if present in the map
							if (weeklyDataPoints.containsKey(weeklyMapKey)) {
								totalDataPoints = weeklyDataPoints.get(weeklyMapKey);
								tempTotal = weeklyAverageTempMap.get(weeklyMapKey);
							} else {
								tempTotal = 0;
								totalDataPoints = 0;
							}
							// add the current data point
							tempTotal += sData.getValue();
							totalDataPoints++;

							// update the average temp
							weeklyAverageTempMap.put(weeklyMapKey, tempTotal / totalDataPoints);
						}
					}
				}
			}
		}

		// return the final data
		return weeklyAverageTempMap;
	}

}
