package com.temparature.morpher;

import org.springframework.stereotype.Service;

import com.temparature.beans.TempDataFromSensor;

/**
 * this class acts a data modification and verification layer 
 * between the db interaction layer and api controllers.
 * this contains the business logic 
 * 
 * @author shtiwari
 *
 */
@Service
public interface TemperatureDM {
	
	/**
	 * get all the temp for all the locations
	 * @return
	 */
	Object getAllLocationData();

	/**
	 * this gets the temp of 1 particular location
	 * @param locationId
	 * @return
	 */
	Object getTemperatureByLocationId(String locationId);

	/**
	 * this adds new temp for a location
	 * this will also validate the data provided.
	 * 
	 * @param inData
	 * @return
	 */
	Object addNewTemperatureDataPoint(TempDataFromSensor inData);

	
	/**
	 * this method will return average data according to the filter provided for a particular location
	 * @param locationId
	 * @param period 
	 * @return
	 */
	Object getAverageData( String locationId, String period ) ;
	
}
