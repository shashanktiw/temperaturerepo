package com.temp.server.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * this is the starting point of spring boot application
 * @author shtiwari
 *
 */
@SpringBootApplication(scanBasePackages = {"com.temparature"})
public class TemperatureWareHouseApplication {

	public static void main(String[] args) {
		SpringApplication.run(TemperatureWareHouseApplication.class, args);
	}
}
