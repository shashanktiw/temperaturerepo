/**
 * 
 */
package com.temp.data;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import com.temparature.utilities.Util;

/**
 * this class is used to generate the data and push in the mongo db via API
 * calling this will help in testing the API.
 * 
 * @author shtiwari
 *
 */
public class LoadTester {

	private static final String USER_AGENT = "Mozilla/5.0";

	// URLs to GET Data
	private static final String GET_ALL_LOCATION_DATA_URL = "http://localhost:8080/temp";
	private static final String GET_TEMPERATURE_BY_LOCATIONID_URL = GET_ALL_LOCATION_DATA_URL;
	private static final String GET_MONTHLY_AVERAGE_DATA_URL = "http://localhost:8080/temp/delhi/average/monthly";
	private static final String GET_WEEKLY_AVERAGE_DATA_URL = "http://localhost:8080/temp/delhi/average/weekly";
	private static final String GET_DAILY_AVERAGE_DATA_URL = "http://localhost:8080/temp/delhi/average/daily";
	
	// URL to Post Data
	private static final String POST_ADD_NEW_TEMPERATURE_DATA_POINT_URL = "http://localhost:8080/temp";
	
	// to generate random number
	private Random rand = new Random();
	
	// possible locations for test data
	private List<String> locations = new ArrayList<>();
	
	public LoadTester() {
		locations.add("Pune");
		locations.add("Gurgaon");
		locations.add("NewYork");
		locations.add("London");
		locations.add("Moscow");
	}
	
	public static void main(String[] args) throws IOException, JSONException, InterruptedException {
		LoadTester rdg = new LoadTester();
		
		//post data
		rdg.generateJsonAndPost(1, 3, 3, 3, 13);
		
		//fetch data
		String url = GET_TEMPERATURE_BY_LOCATIONID_URL + "/" + rdg.generateRandomLocation();
		rdg.getData( url );
	}
	
	/**
	 * this method is to post data in to the APIs.
	 * this generate random data and takes input about the days that need to be stored.
	 * @param numOfMonth
	 * @param numOfDaysPerMonth
	 * @param numOfHoursPerDay
	 * @param numOfMinsPerHour
	 * @param numOfSecPerMin
	 * @throws IOException
	 * @throws JSONException
	 * @throws InterruptedException
	 */
	public void generateJsonAndPost(int numOfMonth, int numOfDaysPerMonth, int numOfHoursPerDay, int numOfMinsPerHour, int numOfSecPerMin) throws IOException, JSONException, InterruptedException {
		Calendar cal = Calendar.getInstance();
		
		int totalCnt = 0;
		
		int mon=cal.get(Calendar.MONTH);
		//month
		while( numOfMonth>0 ) {
			cal.set(Calendar.MONTH, mon);
			
			//days
			int day = 0;
			while( day < numOfDaysPerMonth ) {
				cal.set(Calendar.DATE, day);
				
				//hours 
				int hr = 0;
				while ( hr < numOfHoursPerDay ) {
					cal.set(Calendar.HOUR_OF_DAY, hr);
					
					//min 
					int min = 0;
					while ( min < numOfMinsPerHour ) {
						cal.set(Calendar.MINUTE, min);
						
						//sec
						int sec = 0;
						while ( sec < numOfSecPerMin ) {
							cal.set(Calendar.SECOND, sec);
							
							//data for post request 
							String location = generateRandomLocation();
							String strDate = Util.DATE_FORMAT.format(cal.getTime());
							float temp = generateRandomTemp();
							
							//post the data
							String dataToPost = generatedJSONString(location, temp, strDate);
							System.out.println( dataToPost );
							sendPOST( dataToPost );
							
							//data post
							totalCnt++;
							//increase the loop counter - sec
							sec++;
						}
						
						//increase the loop counter -- min
						min++;
					}
					
					//increase the loop counter -- hr
					hr++;
				}
				
				
				//increase the loop counter -- day
				day++;
			}
			
			//increase the loop counter -- mon
			mon--;
			numOfMonth--;
		}
		
		System.out.println( "Total values posted :: " + totalCnt );
	}


	/** 
	 * post the data 
	 * 
	 * @param content
	 * @throws IOException
	 * @throws JSONException
	 */
	private void sendPOST(String content) throws IOException, JSONException {
		String url = POST_ADD_NEW_TEMPERATURE_DATA_POINT_URL;
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("Content-Type", "application/json");

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(content);
		wr.flush();
		wr.close();
		
		System.out.println( con.getResponseCode() );
	}
	
	/**
	 * method to fetch data
	 * @throws IOException
	 */
	private void getData(String urlStr ) throws IOException {
		URL obj = new URL(urlStr);
		System.out.println("fetchin data for " + urlStr);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
		int responseCode = con.getResponseCode();
		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println("Return Data Length :: " + response.length());
		} else {
			System.out.println("GET request not worked");
		}

	}

	/**
	 * generate a random number between 0 and 50
	 * ( probable temp )
	 * @return
	 */
	private float generateRandomTemp() {
		float min = (float) 0.0;
		float max = (float) 50.00;
		float x = rand.nextFloat() * (max - min) + min;
		float roundOff = (float) (Math.round(x * 100.0) / 100.0);
	    return roundOff;
	}

	/**
	 * generate a random location from the list 
	 * @return
	 */
	private String generateRandomLocation() {
		int randomIndex = (int)(Math.random()*((locations.size()-1)+1))+0;
	    return locations.get(randomIndex);
	}
	
	/** 
	 * create json string to post 
	 * 
	 * @param location
	 * @param temp
	 * @param timeStamp
	 * @return
	 * @throws JSONException
	 */
	private static String generatedJSONString(String location, float temp, String timeStamp) throws JSONException {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("locationId", location);
		jsonObj.put("temp", temp);
		jsonObj.put("timeStamp", timeStamp);
		
		return jsonObj.toString();
	}
}
