package com.temp.server;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.temparature.beans.TemperatureDataPoint;
import com.temparature.dal.TemperatureDAL;
import com.temparature.morpher.TemperatureDM;
import com.temparature.rest.controller.TemperatureController;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=TemperatureController.class)
@AutoConfigureMockMvc
public class TemperatureControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TemperatureDM tDM;
	
	@MockBean
	private TemperatureDAL tDal;
	
	String exampleTempInputJson = "{" + 
			"	\"locationId\": \"delhi\"," + 
			"	\"temp\": \"13\"," + 
			"	\"timeStamp\": \"2018-10-12 09:25:04\"" + 
			"}";

	@Test
	public void getTempDataForLocationTest() throws Exception {
		String retString = "{\"message\": \"this is mocked response to location data test\"}";
		Mockito.when(tDM.getTemperatureByLocationId(Mockito.anyString())).thenReturn(retString);
		
		this.mockMvc.perform(get("/temp/Delhi")).andExpect(status().isOk())
				.andExpect(content().string(retString));
	}

	@Test
	public void getAllTempDataTest() throws Exception {
		List<TemperatureDataPoint> retList = new ArrayList<>();
		Mockito.when(tDal.getAllLocations()).thenReturn(retList);
		
		//mocking the response
		this.mockMvc.perform(get("/temp")).andExpect(status().isOk()).equals(retList);
	}

	@Test
	public void getAllTempDataMockTest() throws Exception {
		List<TemperatureDataPoint> retList = new ArrayList<>();
		Mockito.when(tDal.getAllLocations()).thenReturn(retList);
		
		//mocking the response
		this.mockMvc.perform(get("/temp")).andExpect(status().isOk()).equals(retList);
	}

}
