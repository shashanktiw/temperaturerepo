## Readme.md

This project is to create APIs that can be used by external sensors to POST temperature data.
starting point is ths TemperatureController.java
All the apis are created here.
This passes data to the TemperatureDM class, which is responsible for validating and manipulating the input according to the DB layer
The next layer is TemperatureDAL, this interacts with the DB. All the needed DB calls are mentioned here.
This takes the input from DM layer and returns the raw data from the DB.  

# MongoDB structure
This is time based document in the mongo db
this will contain data for 1 year.
{
    "locationId": "Delhi",
    "lastPolledTimeStamp": "2018-10-12 09:25:04",
    "tempData": {
        "month_10": {
            "day_12": {
                "hour_9": {
                    "minute_25": {
                        "second_4": 13
                    }
                }
            }
        }
    }
}

# Post API
	http://<server ip : server port>/temp (POST)
POST JSON Sample
	{
		"locationId": "delhi",
		"temp": "13",
		"timeStamp": "2018-10-12 09:25:04"
	}
	
# Get APIs
1.	To fetch all the data
	http://<server_ip : server_port>/temp (GET)
	
2.	To fetch location specific data
	http://<server_ip : server_port>/temp/{location} (GET)
	
3.	To fetch average data over a period
	http://<server_ip : server_port>/temp/{location}/average/{period}	(GET)
	possible values of period are  -- monthly, weekly and daliy
	

# Unit Tests
TemperatureControllerTest.java
This has unit tests for the APIs.
The database interaction is mocked using mockito.
	
# Load testing
	'com.temp.data.LoadTester.java'
can be used. This class have 
	Method : 	generateJsonAndPost		-- to post data, this takes the input about the number of data points that needs to be added
				and	getData  			-- to fetch the specific URL
This needs to run after the application have started i.e. the pre-requiste here is the running of the system and db. 
This will call the APIs then post/get data
This is a stand-alone class which can be  used to insert data in the DB via api calls.
This will indirectly test the APIs working also. 
This could be enhanced to test the actual API response. 

	